execute as @e[type=minecraft:experience_orb] at @s if entity @e[type=minecraft:experience_orb,distance=0.001..2] store result score @s ketsu.xp run data get entity @s Value

tag @e[type=minecraft:experience_orb,tag=ketsu.processed] remove ketsu.processed

execute as @e[type=minecraft:experience_orb] at @s if entity @e[type=minecraft:experience_orb,distance=0.001..2] run function ketsu:xp/test
